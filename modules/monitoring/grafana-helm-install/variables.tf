variable "grafanaDomain" {
  type = string
}

variable "storageClass" {
  type = string
}

variable "prometheusDomain" {
  type = string
}

variable "priority" {
  type = string
}
