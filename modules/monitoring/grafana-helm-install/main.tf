resource "random_password" "grafanaPassword" {

  length = 16
  special = true

  depends_on = [var.priority]
}


resource "kubernetes_secret" "grafanaUser" {
  metadata {
    name      = "grafanauser"
    namespace = "grafana"
  }

  data = {
    username = "admin"
    password = random_password.grafanaPassword.result
  }

  type = "Opaque"

  depends_on = [random_password.grafanaPassword]
}

resource "null_resource" "grafanaInstall" {
 
  #provisioner "local-exec" {
  #  command = "echo \"${templatefile("${path.module}/templates/values.tmpl", {
  #    grafanaDomain      = var.grafanaDomain,
  #    prometheusDomain   = var.prometheusDomain,
  #    storageClass       = var.storageClass,
  #  })}\" > values.yaml"
  #}

  provisioner "local-exec" {
    command = <<EOT
      cat ${path.module}/templates/values.tmpl | \
      sed 's/__grafanaDomain__/${var.grafanaDomain}/g' | \
      sed 's/__prometheusDomain__/${var.prometheusDomain}/g' |\
      sed  's/__storageClass__/${var.storageClass}/g' \
      > ${path.module}/templates/values.yaml;
    EOT
  }

  provisioner "local-exec" {
    command = <<EOT
      helm upgrade --install grafana stable/grafana --namespace grafana -f ${path.module}/templates/values.yaml;
    EOT
  }
  
  depends_on = [kubernetes_secret.grafanaUser]
}

