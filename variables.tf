variable "prometheusDomain" {
  type = string
}

variable "grafanaDomain" {
  type = string
}

variable "tls_cert" {
  type = string
}

variable "tls_key" {
  type = string
} 

variable "storageClass" {
  type = string
  default = "standard"
}