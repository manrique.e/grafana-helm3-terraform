module "monitoring" {
  source = "./modules/monitoring"

  tls_cert           = var.tls_cert
  tls_key            = var.tls_key
  prometheusDomain   = var.prometheusDomain
  grafanaDomain      = var.grafanaDomain
  storageClass       = var.storageClass

}